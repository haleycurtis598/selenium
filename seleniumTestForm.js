const {Builder, By, Key, until} = require('selenium-webdriver');

let driver = new Builder()
    .forBrowser('chrome')
    .build();

driver.get('http://haleycurtis.com/WDV/WDV341/self-posting.php')
    .then(_ =>
driver.findElement(By.id('customerName')).sendKeys('Haley Curtis', Key.RETURN))

    .then(_ =>
driver.findElement(By.id('customerSsn')).sendKeys('999999999', Key.RETURN))

    .then(_ =>
driver.findElement(By.id('phone')).click())

    .then(_ =>
driver.findElement(By.id('submit')).click())
