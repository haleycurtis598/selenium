

const {Builder, By, Key, until} = require('selenium-webdriver');

let driver = new Builder()
    .forBrowser('chrome')
    .build();

driver.get('http://haleycurtis.com/WDV/WDV321/JavaScript-Final/js-final.php')
    .then(_ =>
driver.findElement(By.id('form_name')).sendKeys('Haley', Key.RETURN))

    .then(_ =>
driver.findElement(By.id('form_lastname')).sendKeys('Curtis', Key.RETURN))

    .then(_ =>
driver.findElement(By.id('form_email')).sendKeys('test@testemail.com', Key.RETURN))

.then(_ =>
driver.findElement(By.id('safe')).click())

.then(_ =>
driver.findElement(By.id('form_message')).sendKeys('Test Comment', Key.RETURN))

.then(_ =>
driver.findElement(By.id('form_date')).sendKeys('02/28/2018', Key.RETURN))

.then(_ =>
driver.findElement(By.name('submit')).click())
